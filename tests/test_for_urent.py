import allure
import pytest

from pageObjects.inventoryPage import InventoryPage
from pageObjects.loginPage import LoginPage
from tests.usersData import UsersCreds


@pytest.fixture(scope='function', autouse=True)
def goto_login_page():

    page = LoginPage()
    page.goto_login_page()
    yield page


@pytest.fixture(scope='function')
def valid_login():
    page = LoginPage()
    page.login(UsersCreds.STANDARD_USER.login, UsersCreds.STANDARD_USER.password)
    yield page


@allure.title("Проверка, что перешли на корректный урл.")
def test_check_url():
    page = LoginPage()
    assert page.get_url == page.Url


@allure.title("Проверка, что заблокированный пользователь не может войти в систему.")
@allure.severity(allure.severity_level.CRITICAL)
@allure.story('login')
@pytest.mark.timeout(30)
def test_try_login_by_locked_user():

    page = LoginPage()
    page.login(UsersCreds.LOCKED_USER.login, UsersCreds.LOCKED_USER.password)
    assert page.get_url == page.Url
    assert page.get_error_msg == "Epic sadface: Sorry, this user has been locked out."
    assert page.get_url != InventoryPage().Url


@allure.title("Проверка входа в систему с валидными данными.")
@allure.severity(allure.severity_level.CRITICAL)
@allure.story('login')
@pytest.mark.timeout(30)
@pytest.mark.dependency
def test_valid_user_login(valid_login):

    page = LoginPage()
    assert page.get_url != page.Url
    assert page.get_url == InventoryPage().Url


@allure.title("Проверка добавления товара в корзину.")
@allure.severity(allure.severity_level.CRITICAL)
@pytest.mark.timeout(90)
@pytest.mark.dependency(depends=["test_valid_user_login"])
def test_choose_2_expensive_items(valid_login):

    page = InventoryPage()
    ...
