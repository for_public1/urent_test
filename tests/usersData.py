from dataclasses import dataclass


@dataclass
class User:
    login: str
    password: str


class UsersCreds:

    LOCKED_USER = User('locked_out_user', 'secret_sauce')
    STANDARD_USER = User('standard_user', 'secret_sauce')