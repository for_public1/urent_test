import pytest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from sessionConnections import SessionConnections


@pytest.fixture(scope='session', autouse=True)
def browser():

    driver = webdriver.Chrome()
    driver.maximize_window()
    SessionConnections.browser_driver = driver
    yield driver

