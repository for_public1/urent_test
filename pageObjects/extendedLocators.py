# -*- coding: utf-8 -*-
from typing import Any, Tuple

from selenium.webdriver.common.by import By
from dataclasses import dataclass, field


@dataclass
class PageExtendLocator:
    """Базовый класс расширенных локаторов
    :param str by: Тип локатора
    :param str loc: Локатор
    :param str eng: Контекст на английском
    :param str rus: Контекст на русском
    :param tuple elm_loc: Кортеж
    :return: obj
    """

    by: str = None
    loc: str = None
    name: str = 'extended locator'
    _by: str = field(default=None, init=False, repr=False)
    _loc: str = field(default=None, init=False, repr=False)
    elm_loc: Tuple = field(default=(), init=False, repr=False)

    @property
    def by(self) -> str:
        return self._by

    @by.setter
    def by(self, value: str):
        self._by = value
        self.elm_loc = (value, self.loc)

    @property
    def loc(self) -> str:
        return self._loc

    @loc.setter
    def loc(self, value: str):
        self._loc = value
        self.elm_loc = (self.by, value)

    def __post_init__(self):
        if type(self._by) is not str:
            self._by = By.CSS_SELECTOR
        if type(self._loc) is not str:
            self._loc = 'body'
        self.elm_loc = (self.by, self.loc)
