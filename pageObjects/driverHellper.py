from typing import List

import allure

from sessionConnections import SessionConnections
from pageObjects.extendedLocators import PageExtendLocator


class SeleniumDriver:
    """ Базовый класс для определения инструментов браузера. """

    def __init__(self, driver = None):
        self.driver = driver if driver else SessionConnections().browser_driver

    def goto(self, url) -> 'SeleniumDriver':
        """ Метод перехода по Урл. """
        self.driver.get(url)
        return self

    def __get_element(self, loc: PageExtendLocator):
        return self.driver.find_element(*loc.elm_loc)

    def __get_elements(self, loc: PageExtendLocator):
        return self.driver.find_elements(*loc.elm_loc)

    def input(self, loc: PageExtendLocator, value: str) -> 'SeleniumDriver':
        with allure.step("Ввод данных в поле -> {loc.name}"):
            self.__get_element(loc).send_keys(value)
            return self

    def click(self, loc: PageExtendLocator) -> 'SeleniumDriver':
        with allure.step(f"Клик по элементу -> {loc.name}"):
            self.__get_element(loc).click()
            return self

    @property
    def get_url(self) -> str:
        return self.driver.current_url

    def get_element_text(self, loc: PageExtendLocator) -> str:
        with allure.step(f"Получение текста из элемента -> {loc.name}."):
            return self.__get_element(loc).text

    def get_elements_text(self, loc: PageExtendLocator) -> List[str]:
        with allure.step(f"Получение текста из элементов -> {loc.name}."):
            elems = self.__get_elements(loc)
            return [elem.text for elem in elems]
