from selenium.webdriver.common.by import By

from pageObjects.driverHellper import SeleniumDriver
from pageObjects.extendedLocators import PageExtendLocator as PEL


class InventoryPage(SeleniumDriver):

    def __init__(self):
        super().__init__()
        self.Url = "https://www.saucedemo.com/inventory.html"
        self.ShoppingCartBtnLoc: PEL = PEL(by=By.ID, loc="shopping_cart_container", name="корзина с количеством товаров")
        self.InventoryItemNameLoc: PEL = PEL(by=By.CLASS_NAME, loc="inventory_item_name", name="название предмета")
        self.InventoryItemPriceLoc: PEL = PEL(by=By.CLASS_NAME, loc="inventory_item_price", name="цена предмета")
        self.InventoryItemAddBtnLoc: PEL = PEL(by=By.NAME, loc="add-to-cart-sauce-labs-bolt-t-shirt",
                                               name="кнопка добавить предмет в корзину")
        self.InventoryItemRemoveBtnLoc: PEL = PEL(by=By.NAME, loc="remove-sauce-labs-backpack", name="кнопка убрать предемет из корзины")
        self.ShoppingCartBtn: PEL = PEL(by=By.NAME, loc="", name="")


    # TODO: Необходимо реализовать класс конструктор, который будет описывать продукт и вызвать его,
    #  чтоб отприсовать все продуктовые элементы.
