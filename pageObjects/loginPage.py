import allure
from selenium.webdriver.common.by import By

from pageObjects.driverHellper import SeleniumDriver
from pageObjects.extendedLocators import PageExtendLocator as PEL


class LoginPage(SeleniumDriver):
    def __init__(self):
        super().__init__()
        self.Url = "https://www.saucedemo.com/"
        self.LoginLoc: PEL = PEL(by=By.NAME, loc="user-name", name="поле логин")
        self.PasswordLoc: PEL = PEL(by=By.NAME, loc="password", name="поле пароль")
        self.LoginBtnLoc: PEL = PEL(by=By.NAME, loc="login-button", name="кнопка 'Логин'")
        self.ErrorMsgLoc: PEL = PEL(by=By.CSS_SELECTOR, loc="h3[data-test='error']", name="текст с ошибкой")

    def goto_login_page(self) -> 'LoginPage':
        self.goto(self.Url)
        return self

    @allure.step("Метод логина в систему.")
    def login(self, login: str, password: str) -> 'LoginPage':
        self.input(self.LoginLoc, login)
        self.input(self.PasswordLoc, password)
        self.click(self.LoginBtnLoc)
        return self

    @property
    def get_error_msg(self) -> str:
        return self.get_element_text(self.ErrorMsgLoc)
