from selenium import webdriver


class SessionConnections:
    """ Класс хранения сессий для работы в тестах. """

    def __init__(self):
        self._browser_driver: dict = {'driver': webdriver}

    @property
    def browser_driver(self) -> webdriver:
        """ Получение сессии браузера, для работы с UI в тестах. """
        return self._browser_driver['driver']

    @browser_driver.setter
    def browser_driver(self, value: webdriver):
        self._browser_driver['driver'] = value
